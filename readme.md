Что бы запустить проект требуется сделать слеующие:

1. Выполнить команду composer update

2. Выполнить команду npm i

3. Настроить подключение в файле .env

4. Выполнить команду php artisan key:generate

5. Выполнить команду php artisan migrate

6. Выполнить команду php artisan db:seed

7. После чего можно запускать проект командами php artisan serve, npm run watch-poll
