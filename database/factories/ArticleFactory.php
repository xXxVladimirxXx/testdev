<?php

use App\Article;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Article::class, function (Faker\Generator $faker) {
    // 'name', 'bedrooms', 'bathrooms', 'storeys', 'garages', 'price'
    return [
        'name' => $faker->name,
        'price' => $faker->randomNumber(6),
        'bedrooms' => $faker->numberBetween(1,9),
        'bathrooms' => $faker->numberBetween(1,9),
        'storeys' => $faker->numberBetween(1,9),
        'garages' => $faker->numberBetween(1,9),
    ];
});