<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticleTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            // 'name', 'price', 'bedrooms', 'bathrooms', 'storeys', 'garages'
            ['id' => 1, 'name' => 'The Victoria', 'price' => rand(200000, 600000), 'bedrooms' => '4', 'bathrooms' => '2', 'storeys' => '2', 'garages' => '2',],
            ['id' => 2, 'name' => 'The Xavier', 'price' => rand(200000, 600000), 'bedrooms' => '4', 'bathrooms' => '2', 'storeys' => '1', 'garages' => '2',],
            ['id' => 3, 'name' => 'The Como', 'price' => rand(200000, 600000), 'bedrooms' => '4', 'bathrooms' => '3', 'storeys' => '2', 'garages' => '3',],
            ['id' => 4, 'name' => 'The Aspen', 'price' => rand(200000, 600000), 'bedrooms' => '4', 'bathrooms' => '2', 'storeys' => '2', 'garages' => '2',],
            ['id' => 5, 'name' => 'The Lucretia', 'price' => rand(200000, 600000), 'bedrooms' => '4', 'bathrooms' => '3', 'storeys' => '2', 'garages' => '2',],
            ['id' => 6, 'name' => 'The Toorak', 'price' => rand(200000, 600000), 'bedrooms' => '5', 'bathrooms' => '2', 'storeys' => '2', 'garages' => '2',],
            ['id' => 7, 'name' => 'The Skyscape', 'price' => rand(200000, 600000), 'bedrooms' => '3', 'bathrooms' => '2', 'storeys' => '1', 'garages' => '2',],
            ['id' => 8, 'name' => 'The Clifton', 'price' => rand(200000, 600000), 'bedrooms' => '3', 'bathrooms' => '2', 'storeys' => '2', 'garages' => '1',],
            ['id' => 9, 'name' => 'The Geneva', 'price' => rand(200000, 600000), 'bedrooms' => '4', 'bathrooms' => '3', 'storeys' => '1', 'garages' => '2',],
        ];

        foreach ($items as $item) {
            Article::create($item);
        }
    }
}
