import {router} from "../../app";
import axios from "axios/index";

export default {
  namespaced: true,
  state: {
    data: []
  },
  actions: {
    initData({ commit }) {
     return new Promise((resolve) => {
        axios({
        url: '/api/articles',
        method: 'GET',
        }).then(
          resp => {
              let data  = (resp.data.articles)
              commit('setData', data)
              resolve(true)
          }
        )
     })
    },
    add({ state, commit, dispatch }, data) {
     return new Promise((resolve, reject) => {
      axios({ 
        url: '/api/article',
        data: data,
        method: 'POST',
      }).then(
        this.dispatch('articles/initData')
      )
     })
    },
    update({ state, commit, dispatch }, data) {
      axios({ 
        url: `/api/articles/${data.id}`,
        data: data,
        method: 'PUT',
      }).then(
        this.dispatch('articles/initData')
      )
    },
    delete({ commit }, dataId) {
      return new Promise((resolve, reject) => {
        axios.delete(`/api/articles/${dataId}`)
        .then((response) => {
          this.dispatch('articles/initData')
        })
      })
    },
  },
  mutations: {
    setData(state, data) {
      state.data = data
    }
  },
  getters: {
    getData(state) {
        return state.data
    }
  }
}