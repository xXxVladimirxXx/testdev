import articles from './articles'

export default {
  modules: {
    articles
  }
}
