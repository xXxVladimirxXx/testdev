import Main  from '../Main';
import Articles  from '../components/Articles';
import Home  from '../components/Home';
import Admin  from '../components/admin/Admin';

export const routes = [
    {
        path: '/',
        component: Main,
        children: [
            {
                path: '',
                component: Home,
            },
            {
                path: 'articles',
                component: Articles
            },
            {
                path: 'admin',
                component: Admin
            }
        ],
    }
];
