<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = \DB::table('articles')->get();

        return response()->json([
            'articles' => $articles
        ], 200);
    }

    public function create(Request $request)
    {
        $article = Article::create($request->all());

        return response()->json([
          'article' => $article
        ], 200);
    }
    
    public function edit(Request $request, $id)
    {
        $article = Article::whereId($id)->first();
        $article->update($request->all());

        return response()->json([
          'article' => $article
        ], 200);
    }

    public function delete($id)
    {
        $article = Article::whereId($id)->first();

        $article->delete();

        return response()->json(null, 204);
    }
}
